var express = require('express'),
  app = express(),


  port = process.env.PORT || 3000;

var path = require('path');

var requestjson = require('request-json');
var request = require('request');
//var _ = require('underscore');
//var _s = require('underscore.string');
/* Var*/
var getClientes = "https://api.mlab.com/api/1/databases/rarreola/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var uriNoClientes = "https://api.mlab.com/api/1/databases/rarreola/collections/NoclienteOmontdif?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var monto ="https://api.mlab.com/api/1/databases/rarreola/collections/Monto?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var users ="https://api.mlab.com/api/1/databases/rarreola/collections/Users?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var buroDeCredito="https://jsonplaceholder.typicode.com/users";
var borraNoCliente = "https://api.mlab.com/api/1/databases/rarreola/collections/NoclienteOmontdif";
var apiKey = "?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt"

var bodyParser = require('body-parser');
var lNClientes = require('./noClientes.json');
//var movimientosJSON = require('./movimientosv2.json');
//const _ = require('underscore');
//const fetch = require('node-fetch');
app.listen(port);
app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/public/JS'));
app.use(express.static(__dirname + '/'));
app.use(bodyParser.json()); // body en formato json
app.use(bodyParser.urlencoded({ extended: false })); //body formulario
console.log('Atento y escuchando al puerto: ' + port);

app.get('/', function(req, res){
  res.sendFile(path.resolve(__dirname, 'index.html'));
})


app.post('/datosCliente/dato', (req, resp) => {
      var noCliente = req.body.numCliente
      console.log(req.body)
      var query = '&q={"idcliente":"'+noCliente+'"}';
      var url = monto+query
      const options = { method:'GET',
      uri: url,
      body:req.body,
      json:true,
      headers:{
        'Content-Type':'application/json'
      }}
      request(options, function(error, response, body) {
          if (!error && response.statusCode == 200) {
          console.log(body[0].monto)
          resp.send({monto:body[0].monto})
          }
          else {
            res.status(500).json(response);
          }
        })
      })


app.get('/linkIdCliente', function(req, res) {
  req.session.valid = true;
  res.redirect('/srcCliente/idCliente.html');
});

//Compara los usuarios y Password
app.get('/Users', function(req, res){
  request(users, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      var info = JSON.parse(body)
      res.send(info);
    }
  })
})

app.get('/Login/:user', function(req, res){
const { user } = req.params;
 var query = '&q={"user":"'+user+'"}';
 var url = users+query
 const options = { method:'GET',
 uri: url,
 body:req.body,
 json:true,
 headers:{
   'Content-Type':'application/json'
 }}
 console.log(url)
 request(options, function(error, response, body) {
     if (!error && response.statusCode == 200) {
console.log(body[0])
       res.send({dato:true, pass:body[0].Password});
     }
     else {
       res.status(500).json(response);
     }
   })
 })







//Obtiene Todos registros para contactar
app.get('/Clientes', function(req, res){
  request(getClientes, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      var info = JSON.parse(body)
      res.send(info);
    }
  })
})

//Obtiene los montos
app.get('/Monto', function(req, res){
  request(monto, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      var info = JSON.parse(body)
      res.send(info);
    }
  })
})


app.get('/MontoUno/:idcliente', function(req, res){
const { idcliente } = req.params;
 var query = '&q={"idcliente":"'+idcliente+'"}';
 var url = monto+query
 const options = { method:'GET',
 uri: url,
 body:req.body,
 json:true,
 headers:{
   'Content-Type':'application/json'
 }}
 console.log(url)
 request(options, function(error, response, body) {
     if (!error && response.statusCode == 200) {
       res.send(body);
     }
     else {
       res.status(500).json(response);
     }
   })
 })

 app.get('/noClientesCorreo/:email', function(req, res){
 const { email } = req.params;
  var query = '&q={"email":"'+email+'"}';
  var url = uriNoClientes+query
  const options = { method:'GET',
  uri: url,
  body:req.body,
  json:true,
  headers:{
    'Content-Type':'application/json'
  }}
  console.log(url)
  request(options, function(error, response, body) {
      if (!error && response.statusCode == 200) {
console.log(body)
        res.send([body]);

      }
      else {
        res.status(500).json(response);
      }
    })
  })









//Obtiene los no clientes y montos diferentes
app.get('/noClientes', function(req, res){
  request(uriNoClientes, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      var info = JSON.parse(body)
      res.send(info);
    }
  })
})

//Registra No clientes o quienes requieren un monto mayor
app.post('/noClientes', function(req, res){
  var nuevo = req.body
  const options = { method:'POST',
  uri: uriNoClientes,
  body:req.body,
  json:true,
  headers:{
    'Content-Type':'application/json'
  }}
  console.log(options)
  request(options, function(error, response, body) {
      if (!error && response.statusCode == 200) {
console.log("Éxito")
        res.send({valor:true,msg:"Su registro se ha realizado con exito"});
      }
      else {
        res.status(500).json(response);
      }
    })
  })


  //Actualiza monto autorizado
  app.put('/noClientes/:correo', function(req, res){
  const { correo } = req.params;
   var query = '&q={"correo":"'+correo+'"}';
   var url = uriNoClientes+query
   const options = { method:'PUT',
   uri: url,
   body:req.body,
   json:true,
   headers:{
     'Content-Type':'application/json'
   }}
   console.log(options)
   request(options, function(error, response, body) {
       if (!error && response.statusCode == 200) {
         res.send('Update correctamente');
       }
       else {
         res.status(500).json(response);
       }
     })
   })



//Borra no clientes para no contactar

app.delete('/noClientes/:oid', function(req, res){
const { oid } = req.params;
console.log(req.params)
 var query = '/'+oid+apiKey+'';
 console.log(url)
 var url = borraNoCliente+query
 const options = { method:'DELETE',
 uri: url,
 json:true,
 headers:{
   'Content-Type':'application/json'
 }}
 console.log(options)
 request(options, function(error, response, body) {
     if (!error && response.statusCode == 200) {
       res.send('Hemos borrado su contacto');
     }
     else {
       res.status(500).json(response);
     }
   })
 })




//Validar los clientes en Buró de crédito

app.get('/Buro', function(req, res){
  request(buroDeCredito, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      var info = JSON.parse(body)
      res.send(info);
    }
  })
})

 app.get('/BuroUno/:name', function(req, res){
 const { name } = req.params;
  var query = '&q={"name":"'+name+'"}';
  var url = buroDeCredito+query
  const options = { method:'GET',
  uri: url,
  body:req.body,
  json:true,
  headers:{
    'Content-Type':'application/json'
  }}
  console.log(url)
  request(options, function(error, response, body) {
      if (!error && response.statusCode == 200) {
        res.send(body);
      }
      else {
        res.status(500).json(response);
      }
    })
  })



//recepción de info
  function datosCliente(){
    var noCliente = $('#numCliente').val();
    console.log(noCliente)

  }

//function getClientes() {
// var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
// var request = new XMLHttpRequest();
