#Imagen base
FROM node:latest

#Directorio de la aplicacion
WORKDIR /app

#Copiado de archivos
ADD . /app

#Dependencias
Run npm install


#Puerto que expongo
EXPOSE 3000

#Comando
CMD ["node", "index.js"]
