var validaBuro;

function getBuro() {
 var urlBuro = "https://jsonplaceholder.typicode.com/users";
 var request = new XMLHttpRequest();

 request.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
     validaBuro =request.response;
     procesarBuro();
   }
 }
 request.open("GET", urlBuro, true);
 request.send();
}
function procesarBuro() {
 var JSONBuro = JSON.parse(validaBuro);
 var divTabla = document.getElementById("divTablaBuro");
 var tabla = document.createElement("table");
 var tbody = document.createElement("tbody");
  tabla.classList.add("table");
 tabla.classList.add("table-striped");
  for (var i = 0; i < JSONBuro.length; i++) {
       var nuevaColumna = document.createElement("tr");
console.log(nuevaColumna)
   var columnaNombre = document.createElement("td");
   columnaNombre.innerText = JSONBuro[i].name;
   var columnaUsuario = document.createElement("td");
   columnaUsuario.innerText = JSONBuro[i].username;
   var columnaEmail = document.createElement("td");
      columnaEmail.innerText = JSONBuro[i].email;
      nuevaColumna.appendChild(columnaNombre);
      nuevaColumna.appendChild(columnaUsuario);
      nuevaColumna.appendChild(columnaEmail);
      tbody.appendChild(nuevaColumna);
    }
    tabla.appendChild(tbody);
    divTabla.appendChild(tabla);
   }
